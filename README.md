# Jenkins Workshop

## Let's start by configure a simple pipeline as a code !

###### Instructions:
1. Clone the git repo to your laptop / VM:
```
git clone git@gitlab.com:skywiz-io/jenkins-workshop.git
```
2. Create a new branch with your first name:
```
cd jenkins-workshop
git checkout -b <my-name>
```
3. Create two files: "Jenkinsfile" , "Dockerfile"

##### In the following configurations ensure you're change the name to yours in the appropiate fields **

4. Let's create a simple Dockerfile: 
```
FROM nginx

#MAINTAINER - Identifies the maintainer of the dockerfile.
MAINTAINER or@skywiz.io

#RUN - Runs a command in the container
RUN echo "<body><h1>Welcome your-name</h1></body>" > /usr/share/nginx/html/index.html
```

5. Pipeline as a code (scripted pipeline) - open Jenkinsfile with your favorite editor:
We'll use try and catch block
```
node {
    currentBuild.result = "SUCCESS"
    
    try {

        stage ('Clean Workspace') {
            cleanWs()
        }

        stage('checkout scm') {
            checkout scm
        }

        stage('Build Dockerfile') {
            docker.build('jenkins-workshop')
        }

        stage('Test') {
            sh 'echo Testing ...'
        }
        
         stage('push image to ECR') {
             // URL of the ECR-registry and ID of the AWS creds stored in the jenkins server
             docker.withRegistry('https://657793106363.dkr.ecr.us-east-1.amazonaws.com', 'ecr:us-east-1:ecr-credentials') {  
                docker.image('jenkins-workshop').push('<your-name>')
            }
        }
        
         stage('Deploy') {
             sh 'docker run -d --name <your-name> -p <host-port(for example: 8083)>:80 657793106363.dkr.ecr.us-east-1.amazonaws.com/jenkins-workshop:<your-name>'

             mail body: "project build succeeded: ${env.BUILD_URL}" ,
             from: '<your-mail>',
             subject: 'project build succeeded',
             to: '<your-mail>'
         }
    }

    catch (err) {

        currentBuild.result = "FAILURE"
    
        mail body: "project build error: ${env.BUILD_URL}" ,
        from: '<your-mail>',
        subject: 'project build failed',
        to: '<your-mail>'

        throw err
    }
}
```

6. Push the new changes to the source code repository in your branch
** Be careful and ensure you're push to your branch ! **
```
git add Dockerfile Jenkinsfile
git commit -m "test"
git push origin <branch-name>
```

7. Connect to the [Jenkins server](http://3.93.51.149:8080/)

8. Create a new Jenkins job with your name

9. Integrate your job with the Gitlab repository and branch

10. Run your pipeline job ! :)

# Docker in Docker Hands-On Lab ! :)
## Instructions:

1. Ask Or for server IP address which include Docker installed.

2. Create Dockerfile which will look like below: 
##### Here we actually use a simple debian package and installing the docker binaries
```
FROM debian:stable

#MAINTAINER - Identifies the maintainer of the dockerfile.
MAINTAINER or@skywiz.io

RUN apt-get update -qq \
    && apt-get install -qqy apt-transport-https ca-certificates curl gnupg2 software-properties-common 

RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -

RUN add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"

RUN apt-get update  -qq \
    && apt-get install docker-ce -y
```
3. Build the image:
```
docker build -t <image-name> .
```
4. After the image was created, run the following command which will run the image and integrate the host docker socket to the new image container:
```
docker run --name <your-name> -it -v /var/run/docker.sock:/var/run/docker.sock <image-name> bash
```
5. Just for testing and verifying all work as expected and we've "dind", you can run the following commands:
```
docker ps 
docker pull alpine
```

# Helm Hands-On Lab ! 
## Instructions:
1. connect to bastion server which includes the integration with k8s cluster (Ask Or for the private key)
```
IP Address: 34.74.39.233

ssh -i <private-key> ubuntu@34.74.39.233
```

2. Install MySQL helm chart and go to the following link:
[MySQL chart](https://github.com/helm/charts/tree/master/stable/mysql)

Run:
```
helm install --name <my-release-name> stable/mysql --namespace <my-name>
```

3. Wait all the pods to be up and running:
```
kubectl get pods -n <my-name>
```

4. Delete MySQL release  
```
helm del --purge <my-release-name>
```
